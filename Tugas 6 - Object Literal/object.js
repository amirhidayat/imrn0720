console.log('====== No. 1 (Array to Object) ====== ');
function arrayToObject(arr) {
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
var i = 0;
var len = arr.length;
var obj = {};
while ( i < len ) {
    obj.firstname = arr[i][0];
    obj.lastname = arr[i][1];
    obj.gender = arr[i][2];
    var cekAge = thisYear - arr[i][3];
    if ( cekAge > 0 ) obj.age = cekAge
    else obj.age = "Invalid Birth Year";

    console.log(String(i+1) + '. '+arr[i][0]+' '+arr[i][1]+': ',obj);    
    i++;
}
console.log('');
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
// Error case 
arrayToObject([]) // ""
console.log('');


console.log('====== No. 2 (Shopping Time) ====== ');
function shoppingTime(memberId, money) {
  var ret =  "Mohon maaf, toko X hanya berlaku untuk member saja";
  var produk = [
   {item:"Sepatu Stacattu", harga:1500000 },
   {item:"Baju Zoro", harga:500000 },
   {item:"Baju H&N", harga:250000 },
   {item:"Sweater Uniklooh", harga:175000 },
   {item:"Casing Handphone", harga:50000 },
   ];

   var listPurchased = [];
   var changeMoney = money;

   if ( memberId != '' && money > 0  ) {
     var i = 0;
     var len = produk.length;
     
     while ( i < len ) {
         if ( changeMoney >= produk[i].harga ){
           changeMoney -= produk[i].harga;
           listPurchased.push(produk[i].item);
         }
      i++;
     }
     
     if ( listPurchased.length > 0) {
      ret = {memberId: memberId,money: money,listPurchased: listPurchased,changeMoney: changeMoney };
     }
     
   }

   return ret;
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000));  
  console.log(shoppingTime('234JdhweRxa53', 15000));  
  console.log(shoppingTime());  
  console.log('');


  console.log('====== No. 3 (Naik Angkot) ====== ');
  function naikAngkot(arrPenumpang) {
    var ret = [];
    var lenA = arrPenumpang.length;
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var lenB = rute.length;
    var x = 0 ;
    while (x < lenA) {
      var bayar = 0;         
      if ( arrPenumpang[x][1] != '' ) {
          var y = 0;
          var naik = false;
          while ( y < lenB ){
              if (arrPenumpang[x][1] == rute[y] ) {
                naik = true;    
              }
              if (arrPenumpang[x][2] == rute[y] ) {
                naik = false;
              }  
              if ( naik ) {
                bayar += 2000;
              }
              y++;
            }
          } 
        ret[x] = { penumpang: arrPenumpang[x][0], naikDari: arrPenumpang[x][1], tujuan: arrPenumpang[x][2], bayar: bayar  }
     x++;
    }

    return ret;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

  console.log(naikAngkot([]));  
