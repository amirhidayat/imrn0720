console.log('---------- Soal 1: if-else ----------');
var nama = "John";
var peran = "Werewolf";

if ( nama == "") {
    console.log("Nama harus diisi!");
} else {

   if ( peran == "" ) {
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!"); 
   } else if ( peran == "Penyihir") {
    console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!"); 
} else if ( peran == "Guard") {
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf!"); 
} else if ( peran == "Werewolf") {
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!"); 
}  

}

console.log('');

console.log('---------- Soal 2: switch case ----------');

var tanggal = 21; //assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1;  // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
 


if ( tanggal >= 1 && tanggal <= 31  ) {
    strtanggal = tanggal;
} else { 
    strtanggal = ''; 
}

switch (bulan) 
{
case 1: { strbulan = 'Januari'; break; }
case 2: { strbulan = 'Februari'; break; }
case 3: { strbulan = 'Maret'; break; }
case 4: { strbulan = 'April'; break; }
case 5: { strbulan = 'Mei'; break; }
case 6: { strbulan = 'Juni'; break; }
case 7: { strbulan = 'Juli'; break; }
case 8: { strbulan = 'Agustus'; break; }
case 9: { strbulan = 'September'; break; }
case 10: { strbulan = 'Oktober'; break; }
case 11: { strbulan = 'November'; break; }
case 12: { strbulan = 'Desember'; break; }
default: { strbulan = ''; break; }
}

if ( tahun >= 1900 && tahun <= 2200  ) {
    strtahun = tahun;
} else {
    strtahun = '';
}

if ( strtanggal == '' || strbulan == '' || strtahun == ''  ) {
    console.log('salah masukin data..'); 
} else{
    console.log(strtanggal+' '+ strbulan +' '+ strtahun); 
}

