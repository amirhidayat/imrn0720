console.log('====== No. 1 (Range) ====== ');
function range(startNum, finishNum) {
    if ( startNum && finishNum) {
        var ret = [];
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i++) {
                ret.push(i);
             } 
        } else {
            for (var i = startNum; i >= finishNum; i--) {
                ret.push(i);
             }
        }

    } else { ret = -1  }
  
    return ret
}
console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18))
console.log(range(54, 50)) 
console.log(range()) 
console.log('');


console.log('====== No. 2 (Range with Step) ====== ');
function rangeWithStep(startNum, finishNum, step) {
    
        var ret = [];
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i+=step) {
                ret.push(i);
             } 
        } else {
            for (var i = startNum; i >= finishNum; i-=step) {
                ret.push(i);
             }
        }

    
  
    return ret    
}

console.log(rangeWithStep(1, 10, 2))  
console.log(rangeWithStep(11, 23, 3))  
console.log(rangeWithStep(5, 2, 1))  
console.log(rangeWithStep(29, 2, 4))  
console.log('');


console.log('====== No. 3 (Sum of Range) ====== ');
function sum(startNum, finishNum, step = 1) {
    var total = 0;
    if (startNum) {
        if ( finishNum ) {
            var arr = rangeWithStep(startNum, finishNum, step);
            for (var i = 0; i < arr.length; i++) {
                total += arr[i];
            }
        } else{
            total = startNum;
        }
    } 

    return total    
}
console.log(sum(1,10))  
console.log(sum(5, 50, 2))  
console.log(sum(15,10))  
console.log(sum(20, 10, 2))  
console.log(sum(1))  
console.log(sum())  
console.log('');


console.log('====== No. 4 (Array Multidimensi) ====== ');
function dataHandling(input) {
    for (var i = 0; i < input.length; i++) {
            console.log('Nomor ID: ' + input[i][0]);
            console.log('Nama Lengkap: ' + input[i][1]);
            console.log('TTL: '+ input[i][2] + ' ' + input[i][3]);
            console.log('Hobi: ' + input[i][4]);
            console.log('');
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

dataHandling(input);
console.log('');
 

console.log('====== No. 5 (Balik Kata) ====== ');
function balikKata(kal1) {
    var kal2 = '';
    var max = kal1.length-1;
    for (var i = max; i >= 0; i--) {
        kal2 = kal2 + kal1[i];
     } 
    return kal2;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log('');


console.log('====== No. 6 (Metode Array) ====== ');
function dataHandling2(input) {
    input.splice(4,1,"Pria", "SMA Internasional Metro");
    input[1] = input[1] + ' Elsharaway';
    input[2] = 'Provinsi ' + input[2];
    console.log(input);
    
    var arr_ttl1 = input[3].split("/") ;
    switch (arr_ttl1[1]) 
    {
    case '01': { strbulan = 'Januari'; break; }
    case '02': { strbulan = 'Februari'; break; }
    case '03': { strbulan = 'Maret'; break; }
    case '04': { strbulan = 'April'; break; }
    case '05': { strbulan = 'Mei'; break; }
    case '06': { strbulan = 'Juni'; break; }
    case '07': { strbulan = 'Juli'; break; }
    case '08': { strbulan = 'Agustus'; break; }
    case '09': { strbulan = 'September'; break; }
    case '10': { strbulan = 'Oktober'; break; }
    case '11': { strbulan = 'November'; break; }
    case '12': { strbulan = 'Desember'; break; }
    default: { strbulan = ''; break; }
    }
    console.log(strbulan);

    arr_ttl1.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(arr_ttl1);

    var arr_ttl2 = input[3].split("/") ;
    var ttl_j = arr_ttl2.join("-");
    console.log(ttl_j);

    var nama = String(input[1]);
    var nama15 = nama.slice(0,15);
    console.log(nama15);

}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
console.log('');