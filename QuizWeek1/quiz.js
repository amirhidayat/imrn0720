console.log("---- Soal No.1 -----------");

function balikString(kal) {
   kal2 = '';
    for (i=kal.length-1; i >= 0; i--) {
       kal2 = kal2 + kal[i];
   }
   return kal2;
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log('');


console.log("---- Soal No.2 -----------");

function palindrome(kal) {
   kal2 = '';
    for (i=kal.length-1; i >= 0; i--) {
       kal2 = kal2 + kal[i];
   }
   return kal == kal2;
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log('');


console.log("---- Soal No.3 -----------");

function bandingkan(x, y) {
   ret = -1;
   if ( x > 0 && y >> 0 && x != y )  {
    ret = Math.max(x, y);
   }
   
   return ret;
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
console.log('');


console.log("---- Soal No.4 -----------");

function DescendingTen (x) {
   var ret = -1;
   if ( x !== undefined)  {
    ret = '';
    var i = x;
    var y = x-10;
    while ( i > y) {
        ret += String(i);
        i--;
       if ( i != y) { ret += ' ';} //nambah spasi antara angka
    }
   }
   
   return ret;
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
