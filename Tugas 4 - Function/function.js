console.log('- No. 1 -');
function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak());
console.log('');

console.log('- No. 2 -');
function kalikan(num1, num2) {
    return num1 * num2;
}
 
var num1 = 11;
var num2 = 3;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); 
console.log('');

console.log('- No. 3 -');
function introduce(name, age, address, hobby) {
    var kalimat = 'Nama saya '+ name + ', umur saya '+ age + ' tahun, alamat saya di '+ address + ', dan saya punya hobby yaitu '+ hobby + '!';
    return kalimat;
}
 
var name = "Park Min young"
var age = 33
var address = "Jln. Turangga Bandung"
var hobby = "Drama Korea"
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
console.log('');