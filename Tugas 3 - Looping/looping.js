console.log('------ 1. LOOPING WHILE PERTAMA -----------');
var flag = 2;
while(flag <= 20) { 
  console.log(flag + ' - I love coding');  
  flag+=2;  
}
console.log('');

console.log('------ 1. LOOPING WHILE KEDUA -----------');
var flag = 20;
while(flag >= 2) { 
  console.log(flag + ' - I will become a mobile developer');  
  flag-=2; 
}
console.log('');

console.log('------ 2. LOOPING FOR -----------');
for(var angka = 1; angka <= 20; angka++) {
  if (angka %2 != 0) {
     if (angka %3 != 0) {
      console.log(angka + ' - ' + 'Santai');
     } else {
      console.log(angka + ' - ' + 'I Love Coding');
     }

    } else{
      console.log(angka + ' - ' + 'Berkualitas');
    }

} 
console.log('');

console.log('------ 3. Persegi Panjang -----------');
var y = 1;
while(y <= 4) { 
  var baris = '';
    for(var x = 1; x <= 8; x++) {
      baris = baris + '#';    
    }
    console.log(baris);
  y++; 
}
console.log('');

console.log('------ 4. Membuat Tangga -----------');
var y = 1;
var baris = '';
while(y <= 7) { 
  baris = baris + '#';    
  console.log(baris);
  y++; 
}
console.log('');

console.log('------ 5. Membuat Papan Catur -----------');
var y = 1;
var baris = '';
while(y <= 8) { 
   var baris = '';
    for(var x = 1; x <= 8; x++) {
      if ( (x+y) %2 == 0) { 
        baris = baris + ' '; 
      } else { 
        baris = baris + '#';  
      }
    }  
  console.log(baris);
  y++; 
}