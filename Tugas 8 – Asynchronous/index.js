// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var x = 0;
var waktu = 10000; //10 detik

var loopBooks = function(arr) {
    readBooks(waktu, arr[x],function(waktuku){
        x++;
        waktu = waktuku;
        
        if(x < arr.length) {
            loopBooks(arr);   
        }
    }); 
}

loopBooks(books);