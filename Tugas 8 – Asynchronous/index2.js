var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var x = 0;
var waktu = 10000; //10 detik

var loopBooks2 = function(arr) {
    readBooksPromise(waktu, arr[x])
        .then(function (waktuku) {
            // yay, finish read this book
            x++;
            waktu = waktuku;
            // next book ?
            if(x < arr.length) {
                loopBooks2(arr);   
            } 
        })
        .catch(function (error) {
            console.log(error.message);
         // time up
        });
}
 
// read books promise
loopBooks2(books) 